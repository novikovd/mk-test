<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;

class ArticleCreateCommand extends Command
{
    protected static $defaultName = 'article:create';

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setDescription('Create new article')
            ->addArgument('title', InputArgument::REQUIRED, "The article's title")
            ->addArgument('slug', InputArgument::REQUIRED, "The article's slug")
            ->addArgument('text', InputArgument::REQUIRED, "The article's text");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $artice = new Article();
        $artice->setTitle($input->getArgument('title'));
        $artice->setSlug($input->getArgument('slug'));
        $artice->setContent($input->getArgument('text'));
        $artice->setCreatedAt(new \DateTime());

        $this->entityManager->persist($artice);
        $this->entityManager->flush();
    }
}