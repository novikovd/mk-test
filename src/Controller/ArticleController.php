<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ArticleRepository;

class ArticleController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ArticleRepository */
    private $articleRepository;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->articleRepository = $entityManager->getRepository('App:Article');
    }

    /**
     * @Route("/news/{slug}", name="article")
     */
    public function viewAction($slug)
    {
        $article = $this->articleRepository->findOneBySlug($slug);

        if (! $article) {
            $this->addFlash('error', "Can't find news");

            return $this->redirectToRoute('articles');
        }

        return $this->render('articles/view.html.twig', array(
            'article' => $article
        ));
    }
}
