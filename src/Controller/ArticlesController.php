<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ArticleRepository;

class ArticlesController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ArticleRepository */
    private $articleRepository;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->articleRepository = $entityManager->getRepository('App:Article');
    }

    /**
     * @Route("/", name="articles")
     */
    public function index()
    {
        return $this->render('articles/index.html.twig', [
            'articles' => $this->articleRepository->findAll()
        ]);
    }

}
